<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SinglePageController extends Controller
{
    // This is for handle the route
    public function index() {
        return view('app');
    }
}
