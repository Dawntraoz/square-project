import './bootstrap';
import '@/sass/app.scss';
import Vue from 'vue';
import Vuetify from 'vuetify';

// Route info
import Routes from '@/js/routes.js';

// Main component
import App from '@/js/views/App';

Vue.use(Vuetify);

const app = new Vue({
   el: '#app',
   router: Routes,
   render: h => h(App),
});

export default app;