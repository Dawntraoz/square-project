# Square Project
You can find my website in live here, is my first vue deploy x)
http://vue-square.herokuapp.com/

### 1. Install WampServer
![N|Solid](https://i.imgur.com/aRnPo8o.png)
http://www.wampserver.com/en/#download-wrapper

### 2. Install Composer
![N|Solid](https://getcomposer.org/img/logo-composer-transparent5.png)
https://getcomposer.org/Composer-Setup.exe
Make sure you have installed wamp.
You will be prompted to choose the php version you want to use, **make sure you select atleast php 7.2.14 or higher to match the version that project was created with.**
![N|Strong](https://i.imgur.com/5f7yZZA.png)

### 3. Install node
![N|Solid](https://i.imgur.com/iptWn9F.png)
https://nodejs.org/en/
After you install composer, remember to close and open your terminal again to refresh.

### 4. Copy the cloned project folder into wamp.
If you have installed wamp with default settings, the destination folder will be
1. For x32 bits:
```sh
C:\wamp\www
```
2. For x64 bits
```sh
3. C:\wamp64\www
```
In www we place the cloned folder **square-project**

### 5. Run server and client side
Open a cmd or power shell terminal in the square-project folder and start server.
```sh
npm install
```
```sh
composer global require "laravel/installer"
```
```sh
composer install
```
```sh
php artisan serve
```
Open another terminal for the UI and start the client side.
```sh
npm run watch
```

Now you will see an end-point to open the local website, in my case is:
Laravel development server started: <http://127.0.0.1:8000>
```sh
http://127.0.0.1:8000/
```

### 6. Git
[![N|Solid](https://i.imgur.com/97DLW4K.png)](https://git-scm.com/)
You will need a version control tool to clone the repository.
Download it from: 
https://git-scm.com/downloads

After you have installed git, open a Git Bash in any folder with context click.
![N|Solid](https://i.imgur.com/lHJEwiI.png)

### 7. Clone my public repository in BitBucket
I've commit all my code in my BitBucket repository, you can find it here:
https://bitbucket.org/Dawntraoz/square-project/commits/all

Make sure to clone the project in **C:\wamp64\www**
Use  the following command to clone
```sh
git clone https://bitbucket.org/Dawntraoz/square-project.git
```
![N|Solid](https://i.imgur.com/cB9Iwog.png)

### 8. Install PhpStorm (IDE)
![N|Strong](https://i.imgur.com/29tfs9M.png)
https://www.jetbrains.com/phpstorm/download/#section=windows

